import { createActions, createReducer } from "reduxsauce";
import Immutable from "seamless-immutable";

export const { Types, Creators } = createActions({
  addReminder: ["reminder", "callback"],
  setReminder: ["reminder"],
  setCurrentReminder: ["reminder"],
  editReminder: ["reminder", "callback"],
  setEditReminder: ["reminder"],
  removeReminder: ["reminder"]
});

const reminder = {
  reminders: [],
  currentReminder: {}
};

const INITIAL_STATE = Immutable(Object.assign({}, reminder));

const setReminder = (state = INITIAL_STATE, action) =>
  state.merge({
    reminders: [...state.reminders, { id: action.id, ...action.reminder }]
  });

const setCurrentReminder = (state = INITIAL_STATE, action) => {
  return state.merge({
    currentReminder: action.reminder
  });
};

const removeReminder = (state = INITIAL_STATE, action) => {
  return state.merge({
    reminders: state.reminders.filter(
      (reminder) => reminder.id !== action.reminder.id
    )
  });
};

const setEditReminder = (state = INITIAL_STATE, action) => {
  return state.merge({
    reminders: action.reminders
  });
};

export default createReducer(INITIAL_STATE, {
  [Types.SET_REMINDER]: setReminder,
  [Types.SET_CURRENT_REMINDER]: setCurrentReminder,
  [Types.REMOVE_REMINDER]: removeReminder,
  [Types.SET_EDIT_REMINDER]: setEditReminder
});
