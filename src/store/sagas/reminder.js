import format from "date-fns/format";
import { all, fork, put, takeLatest, call, select } from "redux-saga/effects";
import WeatherService from "src/service/WeatherService";
import { Types } from "src/store/ducks/reminder";
import { v4 as uuidv4 } from "uuid";

function* getWeatherData(reminder, callback) {
  let response;

  try {
    response = yield call(
      WeatherService.findByCityAndDate,
      reminder.city,
      format(reminder.date, "yyyy-MM-dd")
    );

    if (!response?.data?.days?.[0]) {
      callback && callback("There is a problem with the Weather Service.");
      return null;
    }
  } catch (error) {
    callback && callback(error.response.data);
    return null;
  }

  return response.data.days[0];
}

function* asyncAddReminder(action) {
  const apiData = yield call(getWeatherData, action.reminder, action.callback);

  if (!apiData) {
    return;
  }

  yield put({
    type: Types.SET_REMINDER,
    id: uuidv4(),
    reminder: {
      ...action.reminder,
      icon: apiData.icon,
      conditions: apiData.conditions
    }
  });

  action.callback();
}

function* asyncEditReminder(action) {
  const state = yield select();

  const index = state.reminder.reminders.findIndex(
    (reminder) => reminder.id === action.reminder.id
  );

  if (index !== -1) {
    const apiData = yield call(
      getWeatherData,
      action.reminder,
      action.callback
    );

    if (!apiData) {
      return;
    }

    const reminders = Object.assign([], state.reminder.reminders);

    reminders[index] = {
      ...action.reminder,
      icon: apiData.icon,
      conditions: apiData.conditions
    };

    yield put({
      type: Types.SET_EDIT_REMINDER,
      reminders
    });

    action.callback();
  }
}

function* addReminder() {
  yield takeLatest(Types.ADD_REMINDER, asyncAddReminder);
}

function* editReminder() {
  yield takeLatest(Types.EDIT_REMINDER, asyncEditReminder);
}

export default function* root() {
  yield all([fork(addReminder), fork(editReminder)]);
}
