import { all, fork } from "redux-saga/effects";
import reminder from "./reminder";

export default function* rootSaga() {
  yield all([fork(reminder)]);
}
