import axios from 'axios';

const API_KEY = 'SFHZAFT3D8NE2G65RN2E5JJ8S'; //I know it's not secure, but it's just an presentation project.
const BASE_URL = `https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline`;

const WeatherService = {
  findByCityAndDate: (city, date) => {
    return axios.get(`${BASE_URL}/${city}/${date}?key=${API_KEY}&unitGroup=us`);
  }
};

export default WeatherService;
