import clearday from "./clear-day.png";
import clearnight from "./clear-night.png";
import cloudy from "./cloudy.png";
import fog from "./fog.png";
import hail from "./hail.png";
import partlycloudyday from "./partly-cloudy-day.png";
import partlycloudynight from "./partly-cloudy-night.png";
import rainsnowshowersday from "./rain-snow-showers-day.png";
import rainsnowshowersnight from "./rain-snow-showers-night.png";
import rainsnow from "./rain-snow.png";
import rain from "./rain.png";
import showersday from "./showers-day.png";
import showersnight from "./showers-night.png";
import sleet from "./sleet.png";
import snowshowersday from "./snow-showers-day.png";
import snowshowersnight from "./snow-showers-night.png";
import snow from "./snow.png";
import thunderrain from "./thunder-rain.png";
import thundershowersday from "./thunder-showers-day.png";
import thundershowersnight from "./thunder-showers-night.png";
import thunder from "./thunder.png";
import wind from "./wind.png";

const icons = {
  "clear-day": clearday,
  "clear-night": clearnight,
  cloudy: cloudy,
  fog: fog,
  hail: hail,
  "partly-cloudy-day": partlycloudyday,
  "partly-cloudy-night": partlycloudynight,
  "rain-snow-showers-day": rainsnowshowersday,
  "rain-snow-showers-night": rainsnowshowersnight,
  "rain-snow": rainsnow,
  rain: rain,
  "showers-day": showersday,
  "showers-night": showersnight,
  sleet: sleet,
  "snow-showers-day": snowshowersday,
  "snow-showers-night": snowshowersnight,
  snow: snow,
  "thunder-rain": thunderrain,
  "thunder-showers-day": thundershowersday,
  "thunder-showers-night": thundershowersnight,
  thunder: thunder,
  wind: wind
};

export default icons;
