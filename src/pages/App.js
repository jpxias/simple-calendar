import CustomCalendar from 'src/components/CustomCalendar/CustomCalendar';

function App() {
  return (
    <div>
      <CustomCalendar />
    </div>
  );
}

export default App;
