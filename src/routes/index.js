import App from 'src/pages/App';

const Routes = [
  {
    path: '/',
    component: App,
    exact: true
  }
];

export default Routes;
