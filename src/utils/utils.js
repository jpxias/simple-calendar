import addDays from "date-fns/addDays";
import isSunday from "date-fns/isSunday";
import lastDayOfMonth from "date-fns/lastDayOfMonth";
import isSaturday from "date-fns/isSaturday";
import eachDayOfInterval from "date-fns/eachDayOfInterval";
import subDays from "date-fns/subDays";
import previousSunday from "date-fns/previousSunday";
import nextSaturday from "date-fns/nextSaturday";

const getDaysOfMonth = (currentDate) => {
  let days = eachDayOfInterval({
    start: currentDate,
    end: lastDayOfMonth(currentDate)
  });

  if (!isSunday(currentDate)) {
    days = eachDayOfInterval({
      start: previousSunday(currentDate),
      end: subDays(currentDate, 1)
    }).concat(days);
  }

  if (!isSaturday(lastDayOfMonth(currentDate))) {
    days = days.concat(
      eachDayOfInterval({
        start: addDays(lastDayOfMonth(currentDate), 1),
        end: nextSaturday(lastDayOfMonth(currentDate))
      })
    );
  }

  return days;
};

export { getDaysOfMonth };
