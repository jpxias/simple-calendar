import React, { useEffect, useState } from "react";
import format from "date-fns/format";
import subYears from "date-fns/subYears";
import addYears from "date-fns/addYears";
import addMonths from "date-fns/addMonths";
import subMonths from "date-fns/subMonths";
import setMonth from "date-fns/setMonth";
import isSameMonth from "date-fns/isSameMonth";
import isSameDay from "date-fns/isSameDay";
import chunk from "lodash/chunk";
import "./CustomCalendar.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ReminderCreators } from "src/store/ducks/reminder";
import ModalReminder from "src/components/ModalReminder/ModalReminder";
import Changer from "src/components/Changer/Changer";
import ItemDay from "src/components/ItemDay/ItemDay";
import { weekDays } from "src/utils/constants";
import { getDaysOfMonth } from "src/utils/utils";

const mapStateToProps = (state) => ({
  reminders: state.reminder.reminders
});

const mapDispatchToProps = (dispatch) => ({
  ReminderCreators: bindActionCreators(ReminderCreators, dispatch)
});

function CustomCalendar(props) {
  const [days, setDays] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [currentDate, setCurrentDate] = useState(
    new Date(new Date().getFullYear(), new Date().getMonth(), 1)
  );

  useEffect(() => {
    const mountCalendar = () => {
      let days = getDaysOfMonth(currentDate);

      days = days.map((day) => ({
        date: day,
        reminders: props.reminders.filter(
          (reminder) =>
            isSameMonth(reminder.date, day) && isSameDay(reminder.date, day)
        )
      }));

      setDays(days);
    };

    mountCalendar();
  }, [currentDate, props.reminders]);

  const prevYear = () => setCurrentDate(subYears(currentDate, 1));

  const nextYear = () => setCurrentDate(addYears(currentDate, 1));

  const prevMonth = () => {
    const currentMonth = currentDate.getMonth();

    if (currentMonth > 0) {
      setCurrentDate(subMonths(currentDate, 1));
    } else {
      setCurrentDate(subYears(setMonth(currentDate, 11), 1));
    }
  };

  const nextMonth = () => {
    const currentMonth = currentDate.getMonth();

    if (currentMonth < 11) {
      setCurrentDate(addMonths(currentDate, 1));
    } else {
      setCurrentDate(addYears(setMonth(currentDate, 0), 1));
    }
  };

  const openModal = (reminder) => {
    props.ReminderCreators.setCurrentReminder(Object.assign({}, reminder));
    setShowModal(true);
  };

  const onClickDay = (day) => {
    openModal({ date: day.date });
  };

  const onClickReminder = (e, reminder) => {
    e.stopPropagation();
    openModal({
      ...reminder
    });
  };

  const onClickReminderDelete = (e, reminder) => {
    e.stopPropagation();
    props.ReminderCreators.removeReminder(reminder);
  };

  return (
    <>
      <div className="calendar">
        <div className="changer-container">
          <Changer
            prev={prevYear}
            next={nextYear}
            label={currentDate.getFullYear()}
          />
          <Changer
            prev={prevMonth}
            next={nextMonth}
            label={format(currentDate, "MMMM")}
          />
        </div>

        <table cellSpacing={0} cellPadding={5}>
          <tbody>
            <tr>
              {weekDays.map((day) => (
                <th key={day}>{day}</th>
              ))}
            </tr>

            {chunk(days, 7).map((week) => (
              <tr key={`week-${week[0].date.toString()}`}>
                {week.map((day) => (
                  <ItemDay
                    key={day.date.toString()}
                    day={day}
                    onClickDay={onClickDay}
                    onClickReminder={onClickReminder}
                    onClickReminderDelete={onClickReminderDelete}
                    currentDate={currentDate}
                  />
                ))}
              </tr>
            ))}
          </tbody>
        </table>
        <ModalReminder
          showModal={showModal}
          handleCloseModal={() => setShowModal(false)}
        />
      </div>
    </>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomCalendar);
