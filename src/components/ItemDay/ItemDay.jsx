import React from "react";
import ItemReminder from "src/components/ItemReminder/ItemReminder";
import PropTypes from "prop-types";
import "./ItemDay.scss";

const ItemDay = ({
  day,
  onClickDay,
  onClickReminder,
  onClickReminderDelete,
  currentDate
}) => {
  const isDayFromCurrentMonth = day.date.getMonth() === currentDate.getMonth();

  return (
    <td
      key={day.date.toString()}
      onClick={() => onClickDay && onClickDay(day)}
      className="item-day"
    >
      <label className={`${isDayFromCurrentMonth ? "day" : "day-disabled"}`}>
        {day.date.getDate()}
      </label>

      <div className="reminders-list">
        {day.reminders.map((reminder) => (
          <ItemReminder
            key={reminder.id}
            onClickItem={(e) => onClickReminder && onClickReminder(e, reminder)}
            onClickDelete={(e) =>
              onClickReminderDelete && onClickReminderDelete(e, reminder)
            }
            reminder={reminder}
          ></ItemReminder>
        ))}
      </div>
    </td>
  );
};

ItemDay.propTypes = {
  day: PropTypes.object.isRequired,
  onClickDay: PropTypes.func,
  onClickReminder: PropTypes.func,
  onClickReminderDelete: PropTypes.func,
  currentDate: PropTypes.object.isRequired
};

export default ItemDay;
