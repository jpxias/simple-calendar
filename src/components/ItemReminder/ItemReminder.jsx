import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { format } from "date-fns";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import icons from "src/assets/icons";
import "./ItemReminder.scss";
import PropTypes from "prop-types";

const ItemReminder = ({ onClickItem, reminder, onClickDelete }) => {
  return (
    <div className="item-reminder">
      {reminder.icon && (
        <OverlayTrigger
          placement="top"
          overlay={<Tooltip>{reminder.conditions}</Tooltip>}
        >
          <img alt="condition" src={icons[reminder.icon]}></img>
        </OverlayTrigger>
      )}
      <div onClick={onClickItem} className="reminder">
        <span style={{ marginRight: 10 }}>
          {format(reminder.date, "HH:mm': '")}
          {reminder.title}
        </span>
        <FontAwesomeIcon onClick={onClickDelete} icon={faTrash} />
      </div>
    </div>
  );
};

ItemReminder.propTypes = {
  onClickItem: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  reminder: PropTypes.object.isRequired
};

export default ItemReminder;
