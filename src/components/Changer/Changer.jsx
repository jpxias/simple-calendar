import {
  faChevronLeft,
  faChevronRight
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Button from "react-bootstrap/Button";
import "./Changer.scss";
import PropTypes from "prop-types";

const Changer = ({ prev, next, label = "" }) => {
  return (
    <div className="changer">
      <Button variant="outline-primary" onClick={prev}>
        <FontAwesomeIcon icon={faChevronLeft} />
      </Button>
      <span> {label}</span>
      <Button variant="outline-primary" onClick={next}>
        <FontAwesomeIcon icon={faChevronRight} />
      </Button>
    </div>
  );
};

Changer.propTypes = {
  prev: PropTypes.func.isRequired,
  next: PropTypes.func.isRequired,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default Changer;
