import * as Yup from "yup";

const ReminderValidation = Yup.object().shape({
  title: Yup.string().required("Required field.").max(30),
  city: Yup.string().required("Required field."),
  date: Yup.date()
    .transform((value, originalValue) => new Date(originalValue))
    .required("Required field.")
    .min(new Date(1970, 1, 1), "The date must be bigger than 02-01-1970")
    .typeError("Invalid date.")
});

export { ReminderValidation };
