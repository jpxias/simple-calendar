import { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as ReminderCreators } from "src/store/ducks/reminder";
import { Formik } from "formik";
import format from "date-fns/format";
import "./ModalReminder.scss";
import clone from "lodash/clone";
import { ReminderValidation } from "src/components/ModalReminder/validationScheme";

const INITIAL_VALUES = {
  title: "",
  city: "",
  date: ""
};

const mapDispatchToProps = (dispatch) => ({
  ReminderCreators: bindActionCreators(ReminderCreators, dispatch)
});

const mapStateToProps = (state) => ({
  currentReminder: state.reminder.currentReminder
});

const ModalReminder = (props) => {
  const [error, setError] = useState("");

  useEffect(() => {
    setError("");
  }, [props.showModal]);

  const handleSubmit = (values) => {
    const reminder = {
      ...values,
      date: new Date(values.date)
    };

    const callback = (error) =>
      error ? setError(error) : props.handleCloseModal();

    if (values.id) {
      props.ReminderCreators.editReminder(reminder, callback);
    } else {
      props.ReminderCreators.addReminder(reminder, callback);
    }
  };

  const getInitialValues = () => {
    return props.currentReminder.date
      ? {
          ...INITIAL_VALUES,
          ...props.currentReminder,
          date: format(props.currentReminder.date, "yyyy-MM-dd'T'HH:mm")
        }
      : INITIAL_VALUES;
  };

  return (
    <Modal
      className="modal-reminder"
      show={props.showModal}
      onHide={props.handleCloseModal}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>New Reminder</Modal.Title>
      </Modal.Header>
      <Formik
        initialValues={getInitialValues()}
        enableReinitialize={true}
        validateOnChange={false}
        validateOnBlur={false}
        validationSchema={ReminderValidation}
        onSubmit={(values) => {
          handleSubmit(clone(values));
        }}
      >
        {(fProps) => (
          <form onSubmit={fProps.handleSubmit}>
            <Modal.Body>
              <Form.Control
                type="text"
                name="title"
                placeholder="Title"
                value={fProps.values.title}
                onChange={fProps.handleChange}
              />
              <span className="error">{fProps.errors.title}</span>

              <Form.Control
                type="text"
                value={fProps.values.city}
                onChange={fProps.handleChange}
                name="city"
                placeholder="City"
              />
              <span className="error">{fProps.errors.city}</span>

              <Form.Control
                type="datetime-local"
                value={fProps.values.date}
                onChange={fProps.handleChange}
                name="date"
              />
              <span className="error">{fProps.errors.date}</span>
            </Modal.Body>
            <div style={{ padding: 10 }}>
              <span className="error">{error}</span>
            </div>
            <Modal.Footer>
              <Button onClick={props.handleCloseModal}>Close</Button>
              <Button type="submit">Confirm</Button>
            </Modal.Footer>
          </form>
        )}
      </Formik>
    </Modal>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalReminder);
