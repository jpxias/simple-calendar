# simple-calendar

Simple Calendar with reminders creation and weather API integration made using React.

## Node Version Used:
`12.4.0`

## Installing dependencies
`npm install`

## Running
`npm start`
